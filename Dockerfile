FROM debian:10

EXPOSE 80
RUN apt-get update
RUN apt-get install -y apache2 libapache2-mod-php \
    php-gd \
    php-json \
    php-mysql \
    php-sqlite3 \
    php-curl \
    php-intl \
    php-imagick \
    php-zip \
    php-xml \
    php-mbstring \
    php-soap \
    php-ldap \
    php-apcu \
    php-redis \
    php-dev \
    libsmbclient-dev \
    php-gmp \
    smbclient 

RUN rm -rf /var/lib/apt/lists/*
WORKDIR /var/www
ADD https://download.owncloud.com/server/stable/owncloud-complete-latest.tar.bz2 .
RUN tar -xjf owncloud-complete-latest.tar.bz2
WORKDIR /var/www/owncloud
RUN cp -r * /var/www/html
RUN chown -R www-data. /var/www/html
ENTRYPOINT [ "/usr/sbin/apache2ctl" ]
CMD [ "-D", "FOREGROUND" ]
